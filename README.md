# AWS S3 Bucket Deployment with CDK and CodeWhisperer

This project demonstrates the generation and deployment of an Amazon S3 bucket using the AWS Cloud Development Kit (CDK), with Amazon CodeWhisperer helping guide the coding. It showcases how to leverage CodeWhisperer for rapid development and deployment of cloud resources on AWS, including hosting a static website in an S3 bucket. The website is available at [this URL](https://miniprojectstack-s3ids721projectsd25f998a-s4qheajnkmqv.s3.us-east-1.amazonaws.com/index.html).

## Overview

The AWS CDK provides a high-level framework to define cloud infrastructure in code and provision it through AWS CloudFormation. Combined with the AI-driven code generation capabilities of Amazon CodeWhisperer, this project streamlines the development process of creating, configuring, and deploying AWS resources.

## Setup

To run this project, ensure you have the following prerequisites installed:

- AWS CLI
- Node.js and npm
- AWS CDK Toolkit

Then, clone the repository to your local machine:

```bash
git clone https://gitlab.com/hm222/s3-bucket.git
cd s3-bucket
```

## S3 Bucket Features

The deployed S3 bucket is configured with **versioning**, **encryption**, and **static website hosting** to enhance data protection, retrievability, and to serve web content:

- **Versioning**: Enabled to keep multiple variants of an object in the same bucket, crucial for data recovery and protection against accidental overwrites and deletions.
- **Encryption**: Uses S3-managed encryption keys for automatic data encryption, ensuring secure storage.
- **Static Website Hosting**: Configured to host static web content, making it accessible via a public URL.

## Deployment

Deploy the S3 bucket with the following command, ensuring you have the appropriate AWS credentials and default region configured:

```bash
cdk deploy
```

## Hosting a Static Website

To host your static website on the deployed S3 bucket:

1. **Build The Website**: Use your favorite static site generator (I used zola) to build the website. The output should be a set of static files (HTML, CSS, JavaScript, images).

2. **Hosting a Static Website with CI/CD**

This project supports hosting a static website on the deployed S3 bucket, fully automated through the GitLab CI/CD pipeline. This ensures the website content is automatically built and deployed to S3 whenever changes are pushed to the repository.

### GitLab CI/CD Configuration

The following additions to the `.gitlab-ci.yml` ensure that the static site content is deployed to S3:

```yaml
image: node:latest

stages:
  - lint
  - build
  - deploy
  - sync

...

sync:
  stage: sync
  image: python:latest # Use an image with AWS CLI v2
  script:
    - pip install awscli
    - aws s3 sync portofolio/public/ s3://your-bucket-name/portofolio/ --acl public-read
  only:
    - main
```

### Automated Deployment

With this configuration, every push to the `main` branch triggers the CI/CD pipeline, automatically building the static site and syncing it to the S3 bucket. This process ensures that the website content is always up-to-date without manual intervention.

- **Note**: Ensure you have configured the AWS credentials (`AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_DEFAULT_REGION`) in the GitLab CI/CD settings as protected variables for authentication.

3. **Access The Website**: The website should now be accessible through the S3 bucket's static website hosting endpoint, which can be found in the AWS S3 console under the bucket's properties.

## Usage

The S3 bucket created by this project can be used for various applications, including hosting static web content, storing application data, or archiving.