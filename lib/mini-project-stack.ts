import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as s3 from 'aws-cdk-lib/aws-s3';

export class MiniProjectStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    
    // Create an S3 bucket configured for website hosting
    const bucket = new s3.Bucket(this, 'S3-IDS721-Projects', {
      versioned: true,
      encryption: s3.BucketEncryption.S3_MANAGED,
      websiteIndexDocument: 'index.html',
      publicReadAccess: true
    });

    // Output the website URL
    new cdk.CfnOutput(this, 'https://miniprojectstack-s3ids721projectsd25f998a-s4qheajnkmqv.s3.us-east-1.amazonaws.com/', {
      value: bucket.bucketWebsiteUrl,
    });
  }
}
